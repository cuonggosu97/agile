import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button
} from "react-native";
import Driver from "./src/screens/Driver";
import Passenger from "./src/screens/Passenger";
import GenericContainer from "./src/commons/GenericContainer";

const DriverWithGenericContainer = GenericContainer(Driver)
const PassengerWithGenericContainer = GenericContainer(Passenger)

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDriver: false,
      isPassenger: false
    };
  }

  render() {
    if (this.state.isDriver) {
      return <DriverWithGenericContainer />
    }
    if (this.state.isPassenger) {
      return <PassengerWithGenericContainer />
    }
    return (
      <View style={styles.container}>
        <Button onPress={() => this.setState({ isPassenger: true })} title="Passenger" />
        <Button onPress={() => this.setState({ isDriver: true })} title="Driver" />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 50
  },

})
