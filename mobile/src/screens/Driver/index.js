import React, { Component } from "react";
import {
  StyleSheet,
  StatusBar,
  View,
  Platform,
  Linking,
  ActivityIndicator,
  Image,
  Alert
} from "react-native";
import MapView, { Marker, Polyline } from 'react-native-maps';
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';
import BottomButton from "../../commons/BottomButton";
import _ from "lodash";
import socketIO from "socket.io-client";


export default class Driver extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLookingForPassengers: false,
      passengerFound: false
    };
    this.acceptPassengerRequest = this.acceptPassengerRequest.bind(this)
    this.findPassengers = this.findPassengers.bind(this);
    this.socket = null
  }

  onMapReady = () => {
    let initialRegion = {
      latitude: this.props.latitude,
      longitude: this.props.longitude,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    }
    Platform.OS === 'ios' && this.map.animateToRegion(initialRegion, 0.2); // TODO remove once the initialRegion is fixed in the module
  };

  findPassengers() {
    if (!this.state.isLookingForPassengers) {
      this.setState({ isLookingForPassengers: true })

      this.socket = socketIO.connect('http://192.168.0.100:3000')
      this.socket.on('connect', () => {
        this.socket.emit('lookingForPassenger')
      })

      this.socket.on("taxiRequest", async (routeResponse) => {
        console.log(routeResponse)
        this.setState({
          isLookingForPassengers: false,
          passengerFound: true,
          routeResponse
        })
        await this.props.getRouterDirections(
          routeResponse.geocoded_waypoints[0].place_id
        )
        this.map.fitToCoordinates(this.props.pointCoords, {
          edgePadding: { top: 50, bottom: 50, left: 50, right: 50 }
        }) 
      })
    }
  }

  acceptPassengerRequest() {
    const passengerLocation = this.props.pointCoords[
      this.props.pointCoords.length - 1
    ]

    BackgroundGeolocation.on('location', (location) => {
      //Send driver location to passenger
      this.socket.emit('driverLocation', {
        latitude: location.latitude,
        longitude: location.longitude
      })
    });

    BackgroundGeolocation.checkStatus(status => {
      // you don't need to check status before start (this is just the example)
      if (!status.isRunning) {
        BackgroundGeolocation.start(); //triggers start on start event
      }
    });

    if (Platform.OS === 'ios') {
      Linking.openURL(
        `http:maps.apple.com/?daddr=${passengerLocation.latitude}, ${
        passengerLocation.longitude
        }`
      )
    } else {
      Linking.openURL(
        `https://www.google.com/maps/dir/?api=1&destination=${passengerLocation.latitude}, ${
        passengerLocation.longitude}`
      );
    }
  }

  componentDidMount() {
    BackgroundGeolocation.configure({
      desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
      stationaryRadius: 50,
      distanceFilter: 50,
      debug: false,
      startOnBoot: false,
      stopOnTerminate: true,
      locationProvider: BackgroundGeolocation.ACTIVITY_PROVIDER,
      interval: 10000,
      fastestInterval: 5000,
      activitiesInterval: 10000,
      stopOnStillActivity: false,
    });

    BackgroundGeolocation.on('authorization', (status) => {
      console.log('[INFO] BackgroundGeolocation authorization status: ' + status);
      if (status !== BackgroundGeolocation.AUTHORIZED) {
        // we need to set delay or otherwise alert may not be shown
        setTimeout(() =>
          Alert.alert('App requires location tracking permission', 'Would you like to open app settings?', [
            { text: 'Yes', onPress: () => BackgroundGeolocation.showAppSettings() },
            { text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel' }
          ]), 1000);
      }
    });
  }

  render() {
    let startmarker = null
    let endmarker = null
    let findingPassengerActIndicator = null
    let passengerSearchText = 'FIND PASSENGERS 👥'
    let bottomButtonFuction = this.findPassengers

    if (this.props.latitude === null) return null

    if (this.state.isLookingForPassengers) {
      passengerSearchText = 'FINDING PASSENGERS...'
      findingPassengerActIndicator = (
        <ActivityIndicator
          size='large'
          animating={this.state.isLookingForPassengers}
        />
      )
    }

    if (this.state.passengerFound) {
      passengerSearchText = 'FOUND PASSENGER! ACCEPT RIDE?'
      bottomButtonFuction = this.acceptPassengerRequest
    }

    if (this.props.pointCoords.length > 1) {
      endmarker = (
        <Marker
          coordinate={this.props.pointCoords[this.props.pointCoords.length - 1]}
        >
          <Image
            style={{ width: 40, height: 40 }}
            source={require('../../resources/icons/person-marker.png')}
          />
        </Marker>
      )
    }

    return (
      <View style={styles.container}>
        <StatusBar barStyle='dark-content' />
        <MapView
          ref={map => {
            this.map = map
          }}
          style={styles.map}
          provider='google'
          region={{
            latitude: this.props.latitude,
            longitude: this.props.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
          showsUserLocation={true}
          onMapReady={this.onMapReady}
        >
          <Polyline
            coordinates={this.props.pointCoords}
            strokeWidth={4}
            strokeColor="red"
          />
          {endmarker}
          {startmarker}
        </MapView>
        <BottomButton
          onPressFunction={bottomButtonFuction}
          buttonText={passengerSearchText}
        >
          {findingPassengerActIndicator}
        </BottomButton>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  destinationInput: {
    height: 40,
    marginHorizontal: 5,
    marginTop: 50,
    padding: 5,
    borderWidth: 0.5,
    backgroundColor: 'white'
  },
  suggestions: {
    marginHorizontal: 5,
    padding: 5,
    borderWidth: 0.5,
    backgroundColor: 'white',
    fontSize: 16
  },
  bottomButton: {
    backgroundColor: 'black',
    marginTop: 'auto',
    margin: 20,
    paddingHorizontal: 30,
    paddingVertical: 10,
    alignSelf: 'center'
  },
  bottomButtonText: {
    color: 'white',
    fontSize: 20
  }
})
