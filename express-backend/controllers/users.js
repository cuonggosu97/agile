const User = require('../model/User')

exports.getUser = async (req, res, next) => {
    try {
        const userFromDb = await User.find({})
        const users = userFromDb.map(({ firstName, lastName, email }) => ({
            firstName,
            lastName,
            email
        }))
        return res.json({ users })
    } catch (err) {
        next(err)
    }
}

