import React, { Component } from 'react'
import {
    Text,
    StyleSheet,
    View,
    Platform,
    Alert,
    Image
} from 'react-native'
import LoginForm from "./components/LoginForm";
import axios from "axios";
import baseUrl from "../../commons/baseUrl";
axios.defaults.baseURL = baseUrl

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            errorMessage: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSignIn = this.handleSignIn.bind(this)
        this.handleSignUp = this.handleSignUp.bind(this)
    }

    handleChange(name, value) {
        this.setState({
            [name]: value
        })
    }

    async  handleSignIn() {
        try {
            this.setState({ errorMessage: '' })
            const { email, password } = this.state
            const result = await axios.post('/auth/login', { email, password })
            Alert.alert("", result.data.token)
        } catch (error) {
            this.setState({ errorMessage: error.response.data.message })
        }
    }

    async handleSignUp() {
        try {
            const { email, password } = this.state
            await axios.post('/auth/signup', { email, password })
            this.handleSignIn()
        } catch (error) {
            // console.error(error)
            this.setState({ errorMessage: error.response.data.message })
        }
    }

    render() {
        const { email, password } = this.state
        return (
            <View style={styles.container}>
                <Text style={styles.headerText}>React Native Taxi</Text>
                <LoginForm
                    email={email}
                    password={password}
                    handleChange={this.handleChange}
                    handleSignIn={this.handleSignIn}
                    handleSignUp={this.handleSignUp}
                />
                <Text style={styles.errorMessage}>{this.state.errorMessage}</Text>
                <Image
                    source={require('../../resources/images/greencar.png')}
                    style={styles.logo}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3A3743'
    },
    headerText: {
        fontSize: 44,
        color: '#C1D76D',
        margin: 30,
        textAlign: 'center',
        fontFamily: Platform.OS === 'android' ? "sans-serif-light" : undefined,
        fontWeight: '200'
    },
    errorMessage: {
        marginHorizontal: 18,
        fontSize: 18,
        color: '#F5D7CC',
        fontWeight: 'bold'
    },
    logo: {
        height: 300,
        width: 300,
        alignSelf: 'center'
    }
})
