import React, { Component } from "react";
import {
  TextInput,
  StyleSheet,
  Text,
  StatusBar,
  View,
  TouchableHighlight,
  Platform,
  Image,
  ActivityIndicator
} from "react-native";
import MapView, { Marker, Polyline } from 'react-native-maps';
import BottomButton from "../../commons/BottomButton";
import apiKey from "../../commons/google_api_key";
import _ from "lodash";
import socketIO from "socket.io-client";


export default class Passenger extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      predictions: [],
      isLookingForDriver: false,
      driverIsOnTheWay: false,
      driverLocation: []
    };
    this.onChangeDestinationDebounced = _.debounce(
      this.onChangeDestination, 1000
    )
  }

  onMapReady = () => {
    let initialRegion = {
      latitude: this.props.latitude,
      longitude: this.props.longitude,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    }
    Platform.OS === 'ios' && this.map.animateToRegion(initialRegion, 0.1); // TODO remove once the initialRegion is fixed in the module
  };

  async onChangeDestination(destination) {
    this.setState({ destination });
    const apiUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${destination}&location=${
      this.props.latitude
      },${
      this.props.longitude
      }&radius=2000&key=${apiKey}`
    try {
      const result = await fetch(apiUrl);
      const jsonResult = await result.json();
      this.setState({
        predictions: jsonResult.predictions
      });
    } catch (error) {
      console.log(error)
    }
  }

  async requestDiver() {
    this.setState({ isLookingForDriver: true })
    const socket = socketIO.connect('http://192.168.0.100:3000')

    socket.on('connect', () => {
      console.log('client connected')
      //Request a taxi
      socket.emit('taxiRequest', this.props.routeResponse)
    })

    socket.on('driverLocation', (driverLocation) => {
      const pointCoords = [...this.props.pointCoords, driverLocation]
      this.map.fitToCoordinates(pointCoords, {
        edgePadding: { top: 40, bottom: 20, left: 20, right: 20 }
      })
      this.setState({
        isLookingForDriver: false,
        driverIsOnTheWay: true,
        driverLocation
      })
    })
  }


  render() {
    let marker = null
    let getDriver = null
    let findingDriverActIndicator = null
    let driverMarker = null

    if (this.props.latitude === null) return null

    if (this.state.driverIsOnTheWay) {
      driverMarker = (
        <Marker coordinate={this.state.driverLocation}>
          <Image
            source={require('../../resources/icons/carIcon.png')}
            style={{ width: 40, height: 40 }}
          />
        </Marker>
      )
    }

    if (this.state.isLookingForDriver) {
      findingDriverActIndicator = (
        <ActivityIndicator
          size='large'
          animating={this.state.isLookingForDriver}
        />)
    }

    if (this.props.pointCoords.length > 1) {
      marker = (
        <Marker
          coordinate={this.props.pointCoords[this.props.pointCoords.length - 1]}
        />
      );
      getDriver = (
        <BottomButton
          onPressFunction={() => this.requestDiver()}
          buttonText="REQUEST 🚗"
        >
          {findingDriverActIndicator}
        </BottomButton>
      )
    }

    const predictions = this.state.predictions.map(prediction => (
      <TouchableHighlight
        key={prediction.id}
        onPress={async () => {
          const destinationName = await this.props.getRouterDirections(
            prediction.place_id,
            prediction.structured_formatting.main_text
          );
          this.setState({ predictions: [], destination: destinationName })
          this.map.fitToCoordinates(this.props.pointCoords, {
            edgePadding: { top: 50, bottom: 50, left: 50, right: 50 }
          })
        }
        }

      >
        <Text
          style={styles.suggestions}>
          {prediction.structured_formatting.main_text}
        </Text>
      </TouchableHighlight>

    ))
    return (
      <View style={styles.container}>
        <StatusBar barStyle='dark-content' />
        <MapView
          ref={map => {
            this.map = map
          }}
          style={styles.map}
          provider='google'
          initialRegion={{
            latitude: this.props.latitude,
            longitude: this.props.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
          showsUserLocation={true}
          onMapReady={this.onMapReady}
        >
          <Polyline
            coordinates={this.props.pointCoords}
            strokeWidth={4}
            strokeColor="red"
          />
          {marker}
          {driverMarker}
        </MapView>
        <TextInput
          style={styles.destinationInput}
          placeholder="Enter destination..."
          value={this.state.destination}
          onChangeText={destination => {
            this.setState({ destination });
            this.onChangeDestinationDebounced(destination)
          }}
        />
        {predictions}
        {getDriver}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  destinationInput: {
    height: 40,
    marginHorizontal: 5,
    marginTop: 50,
    padding: 5,
    borderWidth: 0.5,
    backgroundColor: 'white'
  },
  suggestions: {
    marginHorizontal: 5,
    padding: 5,
    borderWidth: 0.5,
    backgroundColor: 'white',
    fontSize: 16
  },
  bottomButton: {
    backgroundColor: 'black',
    marginTop: 'auto',
    margin: 20,
    paddingHorizontal: 30,
    paddingVertical: 10,
    alignSelf: 'center'
  },
  bottomButtonText: {
    color: 'white',
    fontSize: 20
  }
})
