import React, { Component } from "react";
import {
    Keyboard,
    PermissionsAndroid,
    Platform
} from "react-native";
import PolyLine from "@mapbox/polyline";
import apiKey from "./google_api_key";
import Geolocation from '@react-native-community/geolocation';

function genericContainer(WrappedComponent) {
    return class extends Component {
        constructor(props) {
            super(props)
            this.state = {
                latitude: null,
                longitude: null,
                pointCoords: [],

                destination: "",
                routeResponse: {}
            }
            this.getRouterDirections = this.getRouterDirections.bind(this)
        }

        async getRouterDirections(destinationPlaceID, descriptionName) {
            try {
                const respone = await fetch(
                    `https://maps.googleapis.com/maps/api/directions/json?origin=${
                    this.state.latitude
                    },${
                    this.state.longitude
                    }&destination=place_id:${destinationPlaceID}&key=${apiKey}`
                )
                const json = await respone.json()
                const points = PolyLine.decode(json.routes[0].overview_polyline.points)
                const pointCoords = points.map(point => {
                    return {
                        latitude: point[0],
                        longitude: point[1]
                    }
                })
                this.setState({
                    pointCoords,
                    routeResponse: json
                })
                Keyboard.dismiss()
                return descriptionName
            } catch (error) {
                console.log(error)
            }
        }

        async checkAndroidPermissions() {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    {
                        title: "Taxi App",
                        message:
                            "Taxi App needs to use your location to show routes and get taxis"
                    }
                )
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    return true
                } else {
                    return false
                }
            } catch (error) {
                console.warn(error)
            }
        }

        async componentDidMount() {
            let granted = false
            if (Platform.OS === 'ios') {
                granted = true
            } else {
                granted = await this.checkAndroidPermissions()
            }
            if (granted) {
                this.watchId = Geolocation.watchPosition(position => {
                    this.setState({
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                    })
                },
                    error => console.log(error),
                    { enableHighAccuracy: true, timeout: 20000, maximumAge: 2000 }
                )
            }
        }

        componentWillUnmount() {
            Geolocation.clearWatch(this.watchId)
        }

        render() {
            return (
                < WrappedComponent
                    getRouterDirections={this.getRouterDirections}
                    latitude={this.state.latitude}
                    longitude={this.state.longitude}
                    pointCoords={this.state.pointCoords}
                    destination={this.state.destination}
                    routeResponse={this.state.routeResponse}
                />
            )
        }
    }
}

export default genericContainer